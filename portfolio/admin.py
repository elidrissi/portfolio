from django.contrib import admin

# Register your models here.
from .models import Personal, Education, Experience

admin.site.register(Personal)  
admin.site.register(Education)  
admin.site.register(Experience)  
