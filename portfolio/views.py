from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.http import HttpResponse, HttpResponseForbidden
from django.contrib.auth import logout as logout_auth
from django.contrib.auth import login as login_auth
from .models import Personal, Education, Experience, ImageModel
from .forms import PersoForm, UserForm, EducationForm, ExperienceForm, ImageUploadForm
from django.contrib.auth import authenticate
from django.contrib.auth.models import User


def index(request):
    return render(request, 'base.html') 


def login(request):
    if request.method == 'POST':
        form = UserForm(request.POST)
        username = request.POST['username']
        password = request.POST['password']
        new_user = authenticate(username=username, password=password)
        if new_user is not None:
            login_auth(request, new_user)
            return HttpResponseRedirect(reverse('portfolio:perso'))
    else:
        form = UserForm()
        return render(request, 'login.html', {'form': form})

@login_required
def logout(request):
    logout_auth(request)
    return HttpResponseRedirect('/portfolio')

@login_required
def general(request):
    if request.method == 'POST':
        form = PersoForm(request.POST) 
        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            phone = form.cleaned_data['phone']
            status = form.cleaned_data['status']
            address = form.cleaned_data['address']
            cover = form.cleaned_data['cover']
            
            perso = Personal.objects.create(name=name, email=email, phone=phone, status=status, address=address, cover=cover)
            
            return HttpResponseRedirect(reverse('portfolio:show_personal',
                args=(perso.pk,)))

    else:
        form = PersoForm()
    
    return render(request, 'home.html', {'form': form})

@login_required
def education(request, pk):
    u = request.user
    if request.method == 'POST':
        form = EducationForm(request.POST) 
        if form.is_valid():
            year = form.cleaned_data['year']
            title = form.cleaned_data['title']
            desc = form.cleaned_data['desc']
            
            education = u.education_set.create(year=year, title=title, desc=desc)
            return HttpResponseRedirect(reverse('portfolio:show_education',
                args=(education.pk,)))

    else:
        form = EducationForm()
    
    return render(request, 'education.html', {'form': form})


@login_required
def experience(request, pk):
    u = request.user
    if request.method == 'POST':
        form = ExperienceForm(request.POST) 
        if form.is_valid():
            year = form.cleaned_data['year']
            title = form.cleaned_data['title']
            desc = form.cleaned_data['desc']
            
            experience = u.experience_set.create(year=year, title=title, desc=desc)
            return HttpResponseRedirect(reverse('portfolio:show_experience',
                args=(experience.pk,)))

    else:
        form = ExperienceForm()
    
    return render(request, 'experience.html', {'form': form})


def show_personal(request, pk):
    form = get_object_or_404(Personal, pk=pk)
    return render(request, 'show_personal.html', {'form': form})


def show_education(request, pk):
    form = get_object_or_404(Education, pk=pk)
    return render(request, 'show_education.html', {'form': form})


def show_experience(request, pk):
    form = get_object_or_404(Experience, pk=pk)
    return render(request, 'show_experience.html', {'form': form})

def modify_perso(request, pk):
    perso = get_object_or_404(Personal, pk=pk)
    if request.method == 'POST':
        form = PersoForm(request.POST, instance=perso)
        if form.is_valid():
            perso = form.save()

            return HttpResponseRedirect(reverse('portfolio:show_personal',
                args=(perso.pk,)))

    else:
        form = PersoForm(instance=perso)
    
    return render(request, 'home.html', {'form': form})

def modify_education(request, pk):
    education = get_object_or_404(Education, pk=pk)
    if request.method == 'POST':
        form = EducationForm(request.POST, instance=education)
        if form.is_valid():
            education = form.save()

            return HttpResponseRedirect(reverse('portfolio:show',
                args=(education.pk,)))

    else:
        form = EducationForm(instance=education)
    
    return render(request, 'home.html', {'form': form})

def modify_experience(request, pk):
    experience = get_object_or_404(Experience, pk=pk)
    if request.method == 'POST':
        form = ExperienceForm(request.POST, instance=experience)
        if form.is_valid():
            experience = form.save()

            return HttpResponseRedirect(reverse('portfolio:show_experience',
                args=(experience.pk,)))

    else:
        form = ExperienceForm(instance=experience)
    
    return render(request, 'home.html', {'form': form})

def upload_picture(request):
    if request.method == 'POST':
        form = ImageUploadForm(request.POST, request.FILES)
        if form.is_valid():
            newdoc = ImageModel(pic=request.FILES['image'])
            newdoc.save()
            return HttpResponseRedirect(reverse('portfolio:last_step'))
    else:
        form = ImageUploadForm()
    
    return render(request, 'upload_picture.html', {'form': form})

def last_step(request):
    return render(request, 'last.html') 

def first_template(request):
    return render(request, 'resume.html')

