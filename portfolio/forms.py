from django import forms
from django.forms import ModelForm
from . import models as m
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm

class ImageUploadForm(forms.Form):
    image = forms.FileField(label="Select a file")
 
class UserForm(AuthenticationForm):
    class Meta:
        model = User 
        fields = ['username', 'password'] 


class PersoForm(ModelForm):
    class Meta:
        model = m.Personal
        fields = ['name', 'status', 'address', 'email', 'phone', 'cover']
 
class EducationForm(ModelForm):
    class Meta:
        model = m.Education
        fields = ['year', 'title', 'desc']
    
class ExperienceForm(ModelForm):
    class Meta:
        model = m.Experience
        fields = ['year', 'title', 'desc']
 
