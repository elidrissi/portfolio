from django.conf.urls import url

from . import views 

urlpatterns = [
        url(r'^$', views.index, name='index'),
        url(r'^perso/', views.general, name='perso'),
        url(r'^education/(?P<pk>[0-9]+)/$', views.education, name='education'),
        url(r'^experience/(?P<pk>[0-9]+)/$', views.experience, name='experience'),
        url(r'^login/', views.login, name='login'),
        url(r'^logout/', views.logout, name='logout'),
        url(r'^show_personal/(?P<pk>[0-9]+)/$', views.show_personal,
            name='show_personal'),
        url(r'^show_education/(?P<pk>[0-9]+)/$', views.show_education,
            name='show_education'),
        url(r'^show_experience/(?P<pk>[0-9]+)/$', views.show_experience,
            name='show_experience'),
        url(r'^modify_perso/(?P<pk>[0-9]+)/$', views.modify_perso,
            name='modify_perso'),
        url(r'^modify_education/(?P<pk>[0-9]+)/$', views.modify_education,
            name='modify_education'),
        url(r'^modify_experience/(?P<pk>[0-9]+)/$', views.modify_experience, name='modify_experience'),
        url(r'^last_step/', views.last_step,
            name='last_step'),
        url(r'^upload_picture/', views.upload_picture,
            name='upload_picture'),
        url(r'^first_template/', views.first_template,
            name='first_template'),
        ]


