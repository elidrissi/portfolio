from django.db import models
from django.contrib.auth.models import User


class ImageModel(models.Model):
    pic = models.ImageField(upload_to = 'documents/')

class Experience(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    year = models.CharField(max_length=100)
    title = models.CharField(max_length=100)
    desc = models.CharField(max_length=100)


class Personal(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    email = models.EmailField()
    phone = models.CharField(max_length=100)
    status = models.CharField(max_length=100)
    cover = models.TextField(max_length=600)
    

class Education(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    year = models.CharField(max_length=100)
    title = models.CharField(max_length=100)
    desc = models.TextField(max_length=500)



